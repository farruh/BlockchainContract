pragma solidity ^0.4.21;
contract ChequeHolder{   
    mapping(address => Cheque[]) companyCheques;
    mapping(address => mapping(uint=> ChequeProduct[])) chequeProducts;
    
    
    event LogOperation(address sender, string msg );
    
    struct Cheque{        
        uint chequeNumber;
        uint chequeSign; 
        uint totalSum;     
        uint totalCashSum;    
        uint totalEcashSum;    
        uint operationDate;    
        string companyInn;
        string companyName;
        string companyAddress;             
    }

    struct ChequeProduct{   
        string name;
        uint price;
        uint quantity;
        uint totalSum;    
    }    

    function addCheqeu(
            uint _chequeNumber,
            uint _chequeSign,
            uint _totalSum,     
            uint _totalCashSum,
            uint _totalEcashSum,  
            string _companyInn,
            string _companyName,
            string _companyAddress     
        
    ) public {
        //add business logic validation
        
        companyCheques[msg.sender].push(Cheque({
        chequeNumber: _chequeNumber,
        chequeSign: _chequeSign,
        totalSum: _totalSum,
        totalCashSum: _totalCashSum,
        totalEcashSum: _totalEcashSum,   
        operationDate: now,    
        companyInn: _companyInn,
        companyName: _companyName,
        companyAddress: _companyAddress     
        }));
        emit LogOperation(msg.sender, "Cheque created ");
    }
    
    function addChequeProduct(uint _chequeNumber, 
        string _name,
        uint _price,
        uint _quantity,
        uint _totalSum) public {
		
        //validate fields        
        chequeProducts[msg.sender][_chequeNumber].push(ChequeProduct({
            name:  _name,
            price: _price,
            quantity: _quantity,
            totalSum: _totalSum
        }));
        emit LogOperation(msg.sender, "Cheque product created ");
        }
        
    function _getChequeListSize() public view returns (uint){
        return companyCheques[msg.sender].length;
    }
    
    function _getSenderAddress() public  returns (address){
        return msg.sender;
    }
    
    function getChequeList() public view returns (uint[],uint[],uint[]){
        uint[] operationDates;
        uint[] totalSums;
        uint[] chequeNumbers;
        Cheque memory cheque;
        Cheque[] memory curCheques = companyCheques[msg.sender];
        for(uint x = 0; x < curCheques.length; x++) {
            cheque = curCheques[x];
            operationDates.push(cheque.operationDate);
            totalSums.push(cheque.totalSum);
            chequeNumbers.push(cheque.chequeNumber);
        }
        return (operationDates, totalSums, chequeNumbers);
    }
    function getChequeIndex(uint _chequeNumber) public view returns (uint){
		Cheque memory cheque;
        Cheque[] memory curCheques = companyCheques[msg.sender];
        for(uint x = 0; x < curCheques.length; x++) {
            cheque = curCheques[x];
			if(cheque.chequeNumber == _chequeNumber){
				return x+1;//since result is uint (uint >= 0), we can't return not found flag for it we use 0, so when it used as array index it should be -1.
			}			
        }
		return 0;
	}
    function getCheque(uint _chequeNumber) public view returns (uint, uint,uint,uint,uint,uint,string,string,string){
		uint chequeIndex = getChequeIndex(_chequeNumber);
		
		assert(chequeIndex != 0); // it is found
		
		Cheque memory cheque = companyCheques[msg.sender][chequeIndex-1];
		return (cheque.chequeNumber,cheque.chequeSign,cheque.totalSum,cheque.totalCashSum,cheque.totalEcashSum,cheque.operationDate,cheque.companyInn,cheque.companyName,cheque.companyAddress);
		
		
    }
    // since returns string[] not supported, should be implimented returning indexes of array and get every product by index
    function getChequeProduct(uint _chequeNumber) public view returns (string,uint[],uint[],uint[]){
        string memory names; // since string[] not supported as return value using string array concated to one straing with delimiter
        var namesDelimiter = "$!$";
        
        uint[]  prices;
        uint[]  quantities;
        uint[]  totalSums;
        uint  chequeIndex = getChequeIndex(_chequeNumber);
		
		assert (chequeIndex !=0 ); //it is found
		
		ChequeProduct[] chequeProductList = chequeProducts[msg.sender][chequeIndex-1];
		ChequeProduct memory chequeProduct;
		for(uint x = 0; x < chequeProductList.length; x++) {
			chequeProduct = chequeProductList[x];
			names =strConcat(names, chequeProduct.name,namesDelimiter);
			prices.push(chequeProduct.price);
			quantities.push(chequeProduct.quantity);
			totalSums.push(chequeProduct.totalSum);
		}
		return (names, prices, quantities, totalSums);
		
    }
    
    function getTotals() public view returns (uint, uint, uint,uint){
        uint totalAmount=companyCheques[msg.sender].length;
        uint totalSum=0;
        uint totalEcashSum=0;
        uint totalCashSum = 0;
        Cheque memory cheque;
        for(uint x = 0; x < totalAmount; x++) {
            cheque = companyCheques[msg.sender][x];
            totalSum += cheque.totalSum;
            totalCashSum += cheque.totalCashSum;
            totalEcashSum += cheque.totalEcashSum;
        }
        return (totalAmount,totalSum,totalCashSum,totalEcashSum);
    }
    
    
    
    // internal functions
    function strConcat(string _a, string _b, string _c, string _d, string _e) internal pure returns (string){
    bytes memory _ba = bytes(_a);
    bytes memory _bb = bytes(_b);
    bytes memory _bc = bytes(_c);
    bytes memory _bd = bytes(_d);
    bytes memory _be = bytes(_e);
    string memory abcde = new string(_ba.length + _bb.length + _bc.length + _bd.length + _be.length);
    bytes memory babcde = bytes(abcde);
    uint k = 0;
    for (uint i = 0; i < _ba.length; i++) babcde[k++] = _ba[i];
    for (i = 0; i < _bb.length; i++) babcde[k++] = _bb[i];
    for (i = 0; i < _bc.length; i++) babcde[k++] = _bc[i];
    for (i = 0; i < _bd.length; i++) babcde[k++] = _bd[i];
    for (i = 0; i < _be.length; i++) babcde[k++] = _be[i];
    return string(babcde);
}

function strConcat(string _a, string _b, string _c, string _d) internal pure returns (string) {
    return strConcat(_a, _b, _c, _d, "");
}

function strConcat(string _a, string _b, string _c) internal pure returns (string) {
    return strConcat(_a, _b, _c, "", "");
}

function strConcat(string _a, string _b) internal returns (string) {
    return strConcat(_a, _b, "", "", "");
}
}

