module.exports = {
  networks: {
    development: {
      host: "localhost", // localhost
      port: 7545,
      network_id: "*",
    },
  },
  compilers: {
    solc: {
      version: "0.4.21",
    }
  },
};